# 赛题名称
开发三方库“mpchart"

# 三方库介绍
## 功能
MPAndroidChart是一个开源的Android图表库，用于创建各种类型的图表，包括线图、柱状图、饼图等等
## 类型
UI类
## 开发语言
js
## 源码仓
https://github.com/PhilJay/MPAndroidChart

# 赛题描述
基于OpenHarmony4.0 beta2:
- 参考MPAndroidChart源码和接口，使用canvas绘制图片，提供曲线图、柱状图、饼图、饼状图、蜡烛图、气泡图、瀑布图等多种图表类型。
- 定制能力：提供了丰富的 API，允许开发者定制颜色、样式、轴标签等。
- 交互性：支持缩放、拖动和触摸高亮。
- 动画：图表加载和数据变化时提供平滑的动画效果。
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
500分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 功能完备，对标mpchart，支持赛题要求等多种图形显示，接口易用，扩展性强，每支持一个图+10（60%）
- 可靠性强，程序稳定，针对非法数据能正确显示错误（10%）
- 文参考上述作品要求，发布OpenHarmony三方库中心仓(30%)
 

# 赛题导师
- madixin@huawei.com