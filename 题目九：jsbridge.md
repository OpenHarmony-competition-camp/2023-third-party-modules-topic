# 赛题名称
开发三方库“jsbridge"

# 三方库介绍
## 功能
AlipayJSBridge是支付宝小程序提供的JavaScript桥接方案，用于在H5页面和支付宝小程序之间进行交互。
## 类型
工具类
## 开发语言
js
## 源码仓
https://opendocs.alipay.com/open/024kz4

# 赛题描述
基于OpenHarmony4.0 beta2:
- 参考AlipayJSBridge源码和接口，建立H5与artkTS互相调用的机制：

1.jsbridge.call("func",xxx) 从js调用arkts方法

2.jsbridge.post("func",xxx),从arkts调用js方法

# 赛题分数
300分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- jsbridge.call("func",xxx) 从js调用arkts方法(30%)
- jsbridge.post("func",xxx),从arkts调用js方法(30%)
- 扩展其他能力(10%)
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)

# 赛题导师
- madixin@huawei.com