# 赛题名称
开发三方库“glide"

# 三方库介绍
## 功能
Glide是一个非常流行的开源库，用于在Android应用程序中轻松加载和显示图片。它是由Google支持的开源库，具有简单易用的API和高效性能
## 类型
UI类
## 开发语言
java-->js
## 源码仓
https://github.com/bumptech/glide

# 赛题描述
基于OpenHarmony4.0 beta2:
- 参考glide, 针对单个image设置缓存策略，提高加载速度和效率。支持内存缓存和硬盘缓存，支持自定义缓存key。
- 支持自定义转换功能来对图片进行各种自定义操作，例如缩放、旋转、裁剪等等。
- 支持网络图片下载占位符。
- 支持按照应用，ability，动态管理图片的生命周期。
- 性能要求【重要】：List快速刷新多个图片时，支持并发获取和加载图片。
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
500分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 针对单个image设置缓存策略，提高加载速度和效率。支持内存缓存和硬盘缓存，支持自定义缓存key。（20%）
- 支持自定义转换功能来对图片进行各种自定义操作，例如缩放、旋转、裁剪等等 （10%）
- 支持网络图片下载占位符。（10%）
- 支持按照应用，ability，动态管理图片的生命周期（10%）
- 通过List快速滑动和显示多个图片时，支持并发平滑加载和显示图片，不出现白屏和明显卡顿的现象(20%)
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)


# 赛题导师
- madixin@huawei.com