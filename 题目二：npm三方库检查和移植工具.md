# 赛题名称
npm三方库检查和移植工具

# 赛题描述
- 参考[js-e2e工具](https://gitee.com/chenwenjiehw/js-e2e/tree/master/js-compatibiitiy)，或探索其他办法，能够较准确地将npm里的开源三方库移植成OpenHarmony三方库
- 提供文档&样例介绍如何使用这个工具。
- 开源工具到gitee。

# 赛题分数
500分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 功能完备可用，可基于npm仓库top1000测试，输出报告有多少库能直接移植成OpenHarmony三方库，不能移植的有哪些问题，给出汇总和详细信息，指导开发者如何人工整改（60%）
- 文档，样例规范(10%)
- 测试用例规范(10%)
- 开源工具源码到社区，或PR到现有工具(10%)



# 赛题导师
- madixin@huawei.com