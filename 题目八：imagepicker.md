# 赛题名称
开发三方库“imagepicker"

# 三方库介绍
## 功能
一款针对Android平台下的图片选择器，支持从相册获取图片、视频、音频&拍照，支持裁剪(单图or多图裁剪)、压缩、主题自定义配置等功能
## 类型
UI类
## 开发语言
java-->js
## 源码仓
https://github.com/LuckSiege/PictureSelector/blob/version_component/README_CN.md

# 赛题描述
基于OpenHarmony4.0 beta2:
- 以三方库的方式，支持从相册获取图片、视频。
- 支持点击拍摄后，支持裁剪。
- 支持单选，多选媒体文件，并返回媒体文件列表，类似微信发朋友圈前的图片选择。
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
300分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 功能完备，支持选择相册的图片、视频（20%）
- 支持点击拍摄后，支持裁剪。（30%）
- 界面美观，体验性好，可适配不同大小的屏幕 （20%）
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)



# 赛题导师
- madixin@huawei.com