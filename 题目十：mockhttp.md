# 赛题名称
开发三方库“mockhttp"

# 三方库介绍
## 功能
MockHTTP是一个开源的js库，用于模拟HTTP请求和响应。

MockHTTP提供了一个简单易用的API，允许开发者在测试中模拟HTTP请求和响应。它支持模拟不同类型的HTTP请求（GET、POST、PUT、DELETE等），并允许设置请求头、请求体、响应状态码、响应头和响应体等信息。此外，MockHTTP还支持模拟延迟响应和异常响应，以及验证请求和响应的细节。

总之，MockHTTP是一个功能强大的js库，可以帮助开发者在测试中模拟HTTP请求和响应，提高测试的效率和准确性。
## 类型
网络类
## 开发语言
js+C
## 源码仓
https://github.com/ChrisSkeldon/mockHTTP

# 赛题描述
基于OpenHarmony4.0 beta2:
- 对标MockHTTP的能力和接口，提供模拟HTTP请求和响应对象（即分别为HTTP.IncomingMessage和HTTP.ServerResponse的模拟实例）。请求对象将响应“数据”和“结束”事件。
- 可用于网络库xts测试时，模拟http服务，从而不依赖外部服务
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
500分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 按照赛题要求，支持启动http服务，正确处理post/get等请求，支持文件上传下载（40%）
- 接口易用，能对标MockHTTP，在xts测试用例中使用，跑通axios网络库自带的所有xts测试用例 （30%）
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)
 

# 赛题导师
- madixin@huawei.com