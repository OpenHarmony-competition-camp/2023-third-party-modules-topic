# 赛题名称
三方库http接口SDK生成工具

# 赛题描述
- 参考Swagger CodeGen能根据RESTFUL接口定义的swagger.yml，生成能在OpenHarmony4.0 beta2上运行的js sdk三方库，并可直接发布ohpm。
- 网络调用使用axios三方库。
- 提供文档&样例介绍如何使用这个工具。
- 开源工具到gitee。

# 赛题分数
500分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 功能完备可用，可生成Elasticsearch，RabbitMQ等服务的OpenHarmony SDK三方库，发布ohpm三方库中心仓（60%）
- 文档规范(10%)
- 样例规范(10%)
- 测试用例规范(10%)
- 开源工具源码到社区(10%)


# 赛题导师
- madixin@huawei.com