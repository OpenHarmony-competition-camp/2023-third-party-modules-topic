# 赛题名称
开发网络三方库“axiosAdapter"

# 三方库介绍
## 功能
okhttp是安卓开发中常用的网络库。除提供基础网络调用，拦截器功能外，还支持自定义证书，socket连接复用，自定义DNS，连接事件等功能。而目前OpenHarmony三方库axios，仅支持基础网络调用，拦截器的功能，希望能在现有axios的能力基础上补充okhttp相关的能力。
## 类型
网络类
## 开发语言
java->js
## 源码仓
https://square.github.io/okhttp

# 赛题描述
对标okhttp的能力，通过扩展axios adapter，基于OpenHarmony4.0 beta2，实现以下能力：
- 支持https请求时使用本地证书，并支持自定义校验，如只校验证书，而不对域名校验。参考[okhttp证书](https://square.github.io/okhttp/4.x/okhttp-tls/okhttp3.tls/-held-certificate/-builder/#function)
- 支持设置https请求的安全加密套件(加密算法列表) 。
- 支持DNS故障检测、网络时间监听的能力。参考[okhttp event](https://square.github.io/okhttp/features/events/)
- 支持自定义DNS，参考[okhttp 自定义DNS](https://www.jianshu.com/p/6bd131de81d3)
- socket连接复用，提高接口调用性能
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
500分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 支持https请求时使用本地证书，并支持自定义校验，比如只校验证书，而不对域名校验，与axios网络库集成使用（20%）
- 支持设置https请求的安全加密套件(加密算法列表) （5%）
- 支持DNS故障检测、网络时间监听的能力（15%）
- 自定义DNS（15%）
- socket连接复用，提高接口调用性能 (15%)
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)

 

# 赛题导师
- madixin@huawei.com