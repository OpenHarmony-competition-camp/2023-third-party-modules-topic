# 赛题名称
开发三方库“ffmpeg_mobile"

# 三方库介绍
## 功能
simplest_ffmpeg_mobile是一个开源库，包含了多个基于FFmpeg在移动端处理多媒体的简单例子，包括在Android、iOS和WinPhone平台上的例子程序。
## 类型
媒体类
## 开发语言
js + c
## 源码仓
https://github.com/leixiaohua1020/simplest_ffmpeg_mobile

# 赛题描述
基于OpenHarmony4.0 beta2:
- 对标simplest_ffmpeg_mobile，通过aki/napi，对外提供媒体功能，如音视频播放，编解码，裁剪等。
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
300分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 支持视频播放等功能，用户接口易用 （20%）
- 支持视频编解码等功能，用户接口易用 （20%）
- 支持视频裁剪等功能，用户接口易用 （20%）
- 可靠性，对于非法格式能提示错误信息，不崩溃（10%）
- 文档，样例规范(10%)
- 测试用例规范(10%)
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)
 

# 赛题导师
- madixin@huawei.com