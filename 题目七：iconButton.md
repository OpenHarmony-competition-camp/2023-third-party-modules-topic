# 赛题名称
开发三方库“iconButton"

# 三方库介绍
## 功能
iconButton是Flutter,IOS等应用开发时常用的带图片的按钮组件
## 类型
UI类
## 开发语言
js
## 源码仓
https://www.yiibai.com/flutter/flutter-iconbutton.html

# 赛题描述
基于OpenHarmony4.0 beta2:
- 对标Flutter,IOS的图片按钮组件，支持是否显示图片，设置图片的位置，颜色，背景，大小等。
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
300分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 按照赛题要求，功能完备（60%）
- 组件方便易用，参数容易理解 （10%）
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)
 

# 赛题导师
- madixin@huawei.com