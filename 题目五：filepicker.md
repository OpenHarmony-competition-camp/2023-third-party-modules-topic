# 赛题名称
开发三方库“filepicker"

# 三方库介绍
## 功能
文件选择器，可以灵活地选择图像和视频
## 类型
UI类
## 开发语言
java-->js
## 源码仓
https://github.com/DroidNinja/Android-FilePicker

# 赛题描述
基于OpenHarmony4.0 beta2:
- 以三方库的方式，提供从系统相册获取文件的功能，并支持按照类型区分压缩文件，音视频，文本，文档等。
- 支持单选，多选文件，并返回文件列表。
- 提供文档&样例介绍如何使用这个三方库。
- 发布到[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 赛题分数
300分

# 学习资料
- [如何贡献OpenHarmony三方库](https://docs.qq.com/doc/DVmJVTFZpaGt0T0xL)
- [OpenHarmony应用入门与基础课程](https://www.openharmony.cn/courses/application_basic)
- [js移植实例参考](https://gitee.com/openharmony-sig/ohos_axios)


# 作品要求
详见[OpenHarmony三方库作品提交要求](https://docs.qq.com/doc/DRHNIY3RpbWZMREVI)

# 评分标准
参考上述OpenHarmony三方库作品提交要求：
- 功能完备，支持单选和多选文件，提供文件分类功能（50%）
- 界面美观，体验性好，可适配不同大小的屏幕 （20%）
- 参考上述作品要求，发布OpenHarmony三方库中心仓(30%)


# 赛题导师
- madixin@huawei.com